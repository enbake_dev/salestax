require 'rails_helper'

RSpec.describe ReceiptsController, type: :controller do
  
  it "should test the first case" do
    post :create, { :receipt => {:line_items_attributes => {"0" => {:name => "book", :quantity => "1", :price => "12.49"},
                                 "1"=>{ :name => "music cd", :quantity => "1", :price => "14.99"},
                                 "2"=>{ :name => "chocolate bar", :quantity => "1", :price =>"0.85"}}}}
    expect(Receipt.last.total).to eq(29.83)
    expect(Receipt.last.sales_tax).to eq(1.50)
  end
  
  it "should test the second case" do
    post :create, { :receipt => {:line_items_attributes => {"0" => {:name => "imported box of chocolates", :quantity => "1", :price => "10.00"},
                                 "1"=>{ :name => "imported bottle of perfume", :quantity => "1", :price => "47.50"}}}}
    expect(Receipt.last.total).to eq(65.15)
    expect(Receipt.last.sales_tax).to eq(7.65)
  end
  
  it "should test the third case" do
    post :create, { :receipt => {:line_items_attributes => {"0" => {:name => "imported bottle of perfume", :quantity => "1", :price => "27.99"},
                                 "1"=>{ :name => "bottle of perfume", :quantity => "1", :price => "18.99"},
                                 "2"=>{ :name => "packet of headache pills", :quantity => "1", :price =>"9.75"},
                                 "3"=>{ :name => "box of imported chocolates", :quantity => "1", :price =>"11.25"}}}}
    expect(Receipt.last.total).to eq(74.68)
    expect(Receipt.last.sales_tax).to eq(6.70)
  end
end
