class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.string :name
      t.integer :quantity
      t.float :price
      t.integer :tax_perc
      t.float :tax_amount
      t.references :receipt, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
