class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.float :total
      t.float :sales_tax

      t.timestamps null: false
    end
  end
end
