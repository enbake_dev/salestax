json.array!(@receipts) do |receipt|
  json.extract! receipt, :id, :total, :sales_tax
  json.url receipt_url(receipt, format: :json)
end
