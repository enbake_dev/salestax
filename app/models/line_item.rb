class LineItem < ActiveRecord::Base
  belongs_to :receipt
  
  before_create :finalize_tax
  
  private
  
  def finalize_tax
    arr = ["chocolate", "pill", "book"]
    common_tax = arr.any? { |word| name.downcase.include?(word) } ? 0 : 10
    imported_tax = name.downcase.include?("imported")  ? 5 : 0
    total_tax = common_tax + imported_tax
    self.tax_perc =  total_tax
    calculate_tax
  end
  
  def calculate_tax
    amount = (tax_perc * price)/100
    
    a = ((amount * 100).round(0.2) ) * 0.1
    b = a/0.05
    if b != 0
      final_amount = (((b/10).ceil.to_f) * 5)/100
    else
      final_amount = a/100
    end
    self.tax_amount = final_amount
  end
end
