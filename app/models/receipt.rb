class Receipt < ActiveRecord::Base
  has_many :line_items
  accepts_nested_attributes_for :line_items, :reject_if => proc {|attributes| attributes.all? {|k,v| v.blank?} }
  
  after_create :calculate_tax
  
  def calculate_tax
    tax_amount = line_items.map(&:tax_amount).sum
    total = line_items.map(&:price).sum + tax_amount
    self.sales_tax = tax_amount
    self.total = total
    self.save!
  end
end
